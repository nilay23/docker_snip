FROM ubuntu:latest
MAINTAINER Digital Ocean Technology " info@digitalocean.tech"
RUN apt-get update -y
RUN apt-get install -y python-pip python-dev build-essential
RUN pip install snips-nlu
RUN python -m snips_nlu download en

COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt
EXPOSE 5000
CMD python ./app.py
