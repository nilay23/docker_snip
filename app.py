import io
import json
from flask import Flask
from flask import request

app = Flask(__name__)

with io.open("sample_dataset.json") as f:
    sample_dataset = json.load(f)
from snips_nlu import load_resources, SnipsNLUEngine

load_resources(u"en")
nlu_engine = SnipsNLUEngine()

nlu_engine.fit(sample_dataset)

@app.route('/')
def snip_api():
    query = request.args.get('q')
    unicode(query)
    parsing = nlu_engine.parse(query)
    return json.dumps(parsing, indent=2)
app.run(host = '0.0.0.0', port = 8080)
